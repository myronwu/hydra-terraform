provider "aws" {
  region = var.region
  version = "~> 2.26.0"
}

data "aws_region" "current" {}

locals {
  # 19.03 amis from https://nixos.org/nixos/download.html
  nixos-amis = {
    us-east-1 = "ami-0efc58fb70ae9a217"
    us-east-2 = "ami-0abf711b1b34da1af"
    us-west-1 = "ami-07d126e8838c40ec5"
    us-west-2 = "ami-03f8a737546e47fb0"
  }
}

variable "region" { }
variable "default-vpc-sg-id" { }
variable "cidr-whitelist" { }
variable "ec2-pub-key" { }
variable "ec2-priv-key" { }
variable "hydra-cache-bucket-name" { }
variable "postgres-username" { }
variable "postgres-password" { }
