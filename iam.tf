resource "aws_iam_instance_profile" "hydra" {
  name = "hydra"
  role = aws_iam_role.nix-cache-rw.name
}

resource "aws_iam_role" "nix-cache-rw" {
  name = "nix-cache-rw"
  assume_role_policy = <<-EOT
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": "sts:AssumeRole",
                "Principal": {
                   "Service": "ec2.amazonaws.com"
                },
                "Effect": "Allow",
                "Sid": ""
            }
        ]
    }
  EOT
}

resource "aws_iam_role_policy" "nix-cache-rw" {
  name = "nix-cache-rw"
  role = "${aws_iam_role.nix-cache-rw.id}"
  policy = <<-EOT
    {
       "Version":"2012-10-17",
       "Statement":[
          {
             "Effect":"Allow",
             "Action":[
                "s3:ListBucket",
                "s3:GetBucketLocation"
             ],
             "Resource":"${aws_s3_bucket.hydra.arn}"
          },
          {
             "Effect":"Allow",
             "Action":[
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:GetObject",
                "s3:GetObjectAcl",
                "s3:DeleteObject"
             ],
             "Resource":"${aws_s3_bucket.hydra.arn}/*"
          }
       ]
    }
  EOT
}
