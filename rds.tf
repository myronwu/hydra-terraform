
resource "aws_db_instance" "hydra" {
  allocated_storage = 20
  storage_type      = "gp2"
  engine            = "postgres"
  engine_version    = "11.4"
  instance_class    = "db.t2.micro"
  name              = "hydra"
  username          = var.postgres-username
  password          = var.postgres-password
}
