region = "us-west-1"
default-vpc-sg-id = "sg-12345abc"
cidr-whitelist = ["10.0.1.1/24"]
hydra-cache-bucket-name = "myron-hydra-test"
ec2-pub-key = "~/.ssh/id_rsa.pub"
ec2-priv-key = "~/.ssh/id_rsa"

postgres-username = "postgres"
postgres-password = "aoeuidhtns"
