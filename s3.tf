resource "aws_s3_bucket" "hydra" {
  bucket = var.hydra-cache-bucket-name
  acl    = "private"
  region = data.aws_region.current.name
}
