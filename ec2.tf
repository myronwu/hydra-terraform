resource "aws_key_pair" "aws-key" {
  key_name   = "aws-key"
  public_key = file(var.ec2-pub-key)
}

resource "aws_instance" "hydra" {
  ami           = local.nixos-amis[data.aws_region.current.name]
  instance_type = "m5.large"
  key_name      = aws_key_pair.aws-key.key_name

  tags = {
    Name = "hydra"
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = 512
    encrypted   = true
  }

  vpc_security_group_ids = [
    aws_security_group.allow_ssh.id
  , aws_security_group.allow_http.id
  , aws_security_group.allow_tls.id
  , var.default-vpc-sg-id
  ]

  iam_instance_profile = aws_iam_instance_profile.hydra.name

  provisioner "file" {
    connection {
      type = "ssh"
      user = "root"
      private_key = file(var.ec2-priv-key)
      host = self.public_dns
    }

    destination = "/etc/nixos/configuration.nix"
    content     = <<-EOT
      { config, pkgs, ...}: {
        imports = [ <nixpkgs/nixos/modules/virtualisation/amazon-image.nix> ];
        ec2.hvm = true;

        time.timeZone = "UTC";

        services.postfix = {
          enable = true;
          setSendmail = true;
        };

        networking.firewall.allowedTCPPorts = [
          22
          80
          443
        ];

        nixpkgs.overlays = [
          (self: super: {
            hydra = super.hydra.overrideAttrs (oldAttrs: {
              # turn off restricted evaluation mode as it doesn't play well with
              # yarn2nix, callCabal2Nix and various other things
              postPatch = ''
                substituteInPlace src/hydra-eval-jobs/hydra-eval-jobs.cc \
                  --replace 'evalSettings.restrictEval = true;' \
                            'evalSettings.restrictEval = false;'

                substituteInPlace src/lib/Hydra/Plugin/BitBucketStatus.pm \
                  --replace 'next unless $input->emailresponsible;' \
                            'next unless (defined $input) && $input->emailresponsible;'
              '';

              # enable recursive submodules
              postInstall = ''
                substituteInPlace $out/bin/nix-prefetch-git \
                  --replace 'git submodule init' 'git submodule update --init --recursive'
              '' + oldAttrs.postInstall;
            });
          })
        ];

        services.hydra = {
          enable = true;
          useSubstitutes = true;
          hydraURL = "https://${self.public_dns}";
          listenHost = "localhost";
          smtpHost = "localhost";
          notificationSender = "hydra@soostone.com";

          buildMachinesFiles = [];

          minimumDiskFree = 1;
          minimumDiskFreeEvaluator = 1;

          dbi = "dbi:Pg:dbname=${aws_db_instance.hydra.name};host=${aws_db_instance.hydra.address};port=${aws_db_instance.hydra.port};user=${var.postgres-username};password=${var.postgres-password};";

          extraConfig = ''
            store_uri = s3://${aws_s3_bucket.hydra.id}?secret-key=/etc/nix/hydra/secret&log-compression=br&region=${aws_s3_bucket.hydra.region}
            upload_logs_to_binary_cache = true
          '';
        };

        services.nginx = {
          enable = true;
          recommendedProxySettings = true;
          virtualHosts."${self.public_dns}" = {
            forceSSL = true;
            enableACME = true;
            locations."/".proxyPass = "http://localhost:3000";
            extraConfig = ''
              location ~ /(nix-cache-info|.*\.narinfo|nar/*) {
                return 301 https://${aws_s3_bucket.hydra.bucket_regional_domain_name}$request_uri;
              }
            '';
          };
        };

        systemd.services.hydra-manual-setup = {
          description = "Create Admin User for Hydra";
          serviceConfig.Type = "oneshot";
          serviceConfig.RemainAfterExit = true;
          wantedBy = [ "multi-user.target" ];
          requires = [ "hydra-init.service" ];
          after = [ "hydra-init.service" ];
          environment = builtins.removeAttrs (config.systemd.services.hydra-init.environment) ["PATH"];
          script = ''
            if [ ! -e ~hydra/.setup-is-complete ]; then
              # create signing keys
              /run/current-system/sw/bin/install -d -m 551 /etc/nix/hydra
              /run/current-system/sw/bin/nix-store --generate-binary-cache-key hydra /etc/nix/hydra/secret /etc/nix/hydra/public
              /run/current-system/sw/bin/chown -R hydra:hydra /etc/nix/hydra
              /run/current-system/sw/bin/chmod 440 /etc/nix/hydra/secret
              /run/current-system/sw/bin/chmod 444 /etc/nix/hydra/public
              # create cache
              /run/current-system/sw/bin/install -d -m 755 /var/lib/hydra/cache
              /run/current-system/sw/bin/chown -R hydra-queue-runner:hydra /var/lib/hydra/cache
              # done
              touch ~hydra/.setup-is-complete
            fi
          '';
        };

        nix.gc = {
          automatic = true;
          dates = "15 3 * * *";
        };

        nix.autoOptimiseStore = true;

        nix.trustedUsers = ["hydra" "hydra-evaluator" "hydra-queue-runner"];

        nix.buildMachines = [
          {
            hostName = "localhost";
            systems = [ "x86_64-linux" "i686-linux" ];
            maxJobs = 6;
            supportedFeatures = [ ];
          }
        ];
      }
    EOT
  }

  provisioner "remote-exec" {
    connection {
      type = "ssh"
      user = "root"
      private_key = file(var.ec2-priv-key)
      host = self.public_dns
    }

    inline = [
      "nixos-rebuild switch"
    ]
  }
}
